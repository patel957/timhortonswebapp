package sheridan;

import static org.junit.Assert.*;

import java.util.List;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

public class MealsServiceTest {

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}
	
	@Test 
	public void testDrinksRegular() {
		String mealType = "DRINKS";
		MealsService mealService = new MealsService();
		MealType m = MealType.valueOf(mealType);
		List<String> types = MealsService.getAvailableMealTypes( m );
		System.out.println("types happy " + types);
		assertTrue("The type is valid", types!=null);
	}
	
	@Test (expected = NullPointerException.class)
	public void testDrinksException() {
		String mealType = null;
		MealsService mealService = new MealsService();
		MealType m = MealType.valueOf(mealType);
		List<String> types = MealsService.getAvailableMealTypes( m );
		System.out.println("types sad " + types);
		assertTrue("The type is valid", types==null);
	}
	
	@Test
	public void testDrinksBoundaryIn() {
		String mealType = "DRINKS";
		MealsService mealService = new MealsService();
		MealType m = MealType.valueOf(mealType);
		List<String> types = MealsService.getAvailableMealTypes( m );
		System.out.println("types happy " + types);
		assertTrue("The type is valid", types.size()>3);
	}
	
	@Test (expected = NullPointerException.class)
	public void testDrinksBoundaryOut() {
		String mealType = null;
		MealsService mealService = new MealsService();
		MealType m = MealType.valueOf(mealType);
		List<String> types = MealsService.getAvailableMealTypes( m );
		System.out.println("types happy " + types);
		assertTrue("The type is valid", types.size()<2);
	}

	/*@Test
	public void testGetAvailableMealTypes() {
		fail("Not yet implemented");
	}*/

}
